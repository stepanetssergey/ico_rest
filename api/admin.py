from django.contrib import admin
from . import models



admin.site.register(models.UserAPI)
admin.site.register(models.ContractAndToken)

# Register your models here.


@admin.register(models.UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass
