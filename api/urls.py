from django.urls import path
from django.conf.urls import url
from django.conf.urls import include
from rest_framework.routers import DefaultRouter

from . import views

#router = DefaultRouter()
#router.register('user', views.UserAPIView)
#router.register('login', views.LoginViewSet, base_name='login')
#router.register('investinfo/(?P<id>[^/.]+)', views.InvestorInfoNow, base_name='investInfo')
#router.register('investinfo/(?P<address>\w+)',views.InvestorInfoNow, base_name="InvestorInfo")
#router.register('investinfo',views.InvestorInfoNow.as_view())
#router.register('stageinfo',views.StageInfo, base_name='getStageEther')
#router.register('investor/(?P<id>[^/.]+)/',views.InvestorInfo, base_name='getInfoEther')

urlpatterns = [
    #url('^',include(router.urls)),
    url('investinfo/(?P<address>\w+)',views.InvestorInfoNow.as_view()),
    url('stages', views.StageInfo.as_view()),
    url('events', views.PaymentsEvents.as_view()),
    url('events/(?P<address>\w+)', views.PaymentsEventsAddress.as_view()),
    url('abi',views.CurrentTechInfo.as_view()),
    url('address',views.ContractAndToken.as_view())
]
