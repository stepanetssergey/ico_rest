from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from rest_framework import viewsets
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from . import models
from . import serializers
import requests
import json
import logging
from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware
from rest_framework.settings import api_settings
from django.conf import settings
from eth_utils.abi import event_abi_to_log_topic
import json
from django.http import HttpResponse, JsonResponse


logger = logging.getLogger(__name__)



class ContractAndToken(APIView):
    """
    contract and tokens address
    """
    def get(self,request):
        address = models.ContractAndToken.objects.all()
        print(models.ContractAndToken.objects.get(pk=1).contract_address)
        result = serializers.ContractAndTokenSerializer(address, many=True)
        return Response(result.data)

    def post(self,request):
        address = models.ContractAndToken.objects.get(pk=1)
        request_data = request.data
        print(request_data)
        address.contract_address = request_data['contract_address']
        address.token_address = request_data['token_address']
        address.save()
        address1 = models.ContractAndToken.objects.all()
        result = serializers.ContractAndTokenSerializer(address1, many=True)
        return Response(result.data)



class UserAPIView(viewsets.ModelViewSet):

    """
    Add new user with lastname and email
    """

    serializer_class = serializers.UserAPISerializer
    queryset = models.UserAPI.objects.all()


class LoginViewSet(viewsets.ViewSet):

    """Get token from name and password post for auth"""

    serializer_class = AuthTokenSerializer

    def create(self, request):
        return ObtainAuthToken().post(request)

class UpdateEtherSet(viewsets.ModelViewSet):

    """Add fields for getting information from etherscan.io - ethereum address and api kay"""

    serializer_class = serializers.UserAPISerializerUpdate

    def get_queryset(self):
        result = {
                'total_invested':30,
                'locked_amount':20,
                'unlocked_amount':10,
                'dividends':1,
                'last_unlock_date':'2018-09-30'
                }
        return result
    #    return models.UserAPI.objects.all()
    #     current_id = self.kwargs['id']
    #     print('test of id',current_id)
    #     result = models.UserAPI.objects.all().filter(id=current_id)
    #     print(result.values())
    #     return models.UserAPI.objects.all().filter(id=current_id)

    def create(self, request):
    #     current = models.UserAPI.objects.get(id=id)
    #     ether_address = request.data['ether_address']
    #     current.ether_address = ether_address
    #     api_key = request.data['api_key']
    #     current.api_key = api_key
    #     current.save()
    #     return Response({'api_key':api_key,'ether_address':ether_address })
        result = {
                'total_invested':30,
                'locked_amount':20,
                'unlocked_amount':10,
                'dividends':1,
                'last_unlock_date':'2018-09-30'
                }
        return result

class GetInfoFromEtherscan(viewsets.ModelViewSet):
    """
    Get information from etherscan.io
    balance
    current rate ether to USD, timestamp of this rate and so on
    last block of ethereum main net
    """

    serializer_class = serializers.UserAPISerializerUpdate

    def get_queryset(self):
    #     current_id = self.kwargs['id']
    #     print('test of id',current_id)
    #     result = models.UserAPI.objects.all().filter(id=current_id)
    #     print(result.values())
    #     return models.UserAPI.objects.all().filter(id=current_id)
        return models.UserAPI.objects.all()

    def create(self, request, id):

    #     current = models.UserAPI.objects.all().filter(id=id)
    #     print('test of current',current.values('api_key','ether_address'))
    #     for item in current.values('api_key','ether_address'):
    #         api_key = item['api_key']
    #         ether_address = item['ether_address']
    #     #url_for_balance = 'https://api.etherscan.io/api?module=account&action=balance&address='+ether_address + \
    #     #'&tag=latest&apikey='+api_key
    #     get_balance = requests.get('https://api.etherscan.io/api?module=account&action=balance&address=%s&tag=latest&apikey=%s' % (ether_address,api_key)).json()['result']
    #     last_block = requests.get('https://api.etherscan.io/api?module=proxy&action=eth_blockNumber&apikey=%s' % (api_key)).json()['result']
    #     ether_price = requests.get('https://api.etherscan.io/api?module=stats&action=ethprice&apikey=%s' % (api_key)).json()['result']
    #     #get_balance = ''
    #     result = {"balance":get_balance,"last_block":last_block,"current_state":ether_price}

    #     return Response(result)
        result = {
            'total_invested':30,
            'locked_amount':20,
            'unlocked_amount':10,
            'dividends':1,
            'last_unlock_date':'2018-09-30'
            }
        return result

from rest_framework import generics, response
from rest_framework.permissions import IsAuthenticated


class UserView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class UserEtherView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.UserEtherSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user

# class InvestorInfo(generics.RetrieveAPIView):
#     serializer_class = serializers.InvestorsSerializer
#     permission_classes = (IsAuthenticated,)

#     def get_object(self):
#         return self.request.user
#     def create(self, request, id):
#         result = {
#              'total_invested':30,
#              'locked_amount':20,
#              'unlocked_amount':10,
#              'dividends':1,
#              'last_unlock_date':'2018-09-30'
#         }
#         return result
from .serializers import InvestorsSerializer

class InvestorInfoNow(APIView):
    """
    Get information about user by address
    parameters:
    -address: ether address
    """


    @classmethod
    def get_extra_actions(cls):
        return []

    def AbiParsingAndOutput(self,_outName,inputList,abi):
        test_abi = json.loads(abi)

        for item in test_abi:
            if 'name' in item.keys():
                #print(item['name'])
                if item['name'] == _outName:
                    i=0
                    result = {}
                    for in_item in item['outputs']:
                        print(in_item['name'],i)
                        result.update({in_item['name']:inputList[i]})
                        i += 1
        return result

    def get(self, request,address):

        #http_provider = HTTPProvider('https://rinkeby.infura.io/v3/856fa4632365478692da1f0159040f73')
        http_provider = HTTPProvider('https://mainnet.infura.io/v3/856fa4632365478692da1f0159040f73')
        web3 = Web3(http_provider)

        eth_provider = web3.middleware_stack.inject(geth_poa_middleware, layer=0)
        eth_provider = web3.eth
        contract_abi = settings.ABI
        contract_address = settings.DEFAULT_CONTRACT_ADDRESS
        #contract_address = models.ContractAndToken.objects.get(pk=1).contract_address
        contract_instance = eth_provider.contract(abi=contract_abi,address=Web3.toChecksumAddress(contract_address))
        address = Web3.toHex(hexstr=address)
        address = Web3.toChecksumAddress(address)
        #get field from abi

        result = {}
        result_active = contract_instance.call()._investors(Web3.toChecksumAddress(address))
        vesting_date_count = contract_instance.call().vesting_date_count()
        result.update({'contract_address':contract_address,
                       'vasting_date_count':vesting_date_count})
        result.update({'general':self.AbiParsingAndOutput('_investors',result_active,contract_abi)})

        for i in range(1,9):
            current_stage = contract_instance.call()._stage_invest(i, Web3.toChecksumAddress(address))
            current_stage_key = "stage"+str(i)
            print(result.update({current_stage_key:self.AbiParsingAndOutput('_stage_invest',current_stage,contract_abi)}))
        return Response(result)
        #     print(result_active)
        #     total_invest = result_active[0]
        #     total_invest_ether = result_active[1]
        #     active_invest = result_active[3]
        #     locked_amount = result_active[4]
        #     unlocked_amount = result_active[5]
        #     total_withdraw = result_active[6]
        #     signup = result_active[7]
        #
        #
        #     stage_invest = contract_instance.call()._stage_invest(1,Web3.toChecksumAddress(address))
        #     print(stage_invest0)
        #     locked_amount0 = stage_invest0[0]
        #     unlocked_amount0 = stage_invest0[1]
        #     dividends0 = stage_invest0[2]
        #     total_invest_stage0 = stage_invest0[3]
        #
        #     stage_invest1 = contract_instance.call()._stage_invest1(Web3.toChecksumAddress(address))
        #     print(stage_invest1)
        #     locked_amount1 = stage_invest1[0]
        #     unlocked_amount1 = stage_invest1[1]
        #     dividends1 = stage_invest1[2]
        #     total_invest_stage1 = stage_invest1[3]
        #
        #     stage_invest2 = contract_instance.call()._stage_invest2(Web3.toChecksumAddress(address))
        #     print(stage_invest2)
        #     locked_amount2 = stage_invest2[0]
        #     unlocked_amount2 = stage_invest2[1]
        #     dividends2 = stage_invest2[2]
        #     total_invest_stage2 = stage_invest2[3]
        #
        #     stage_invest3 = contract_instance.call()._stage_invest3(Web3.toChecksumAddress(address))
        #     print(stage_invest3)
        #     locked_amount3 = stage_invest3[0]
        #     unlocked_amount3 = stage_invest3[1]
        #     dividends3 = stage_invest3[2]
        #     total_invest_stage3 = stage_invest3[3]
        #
        #     stage_invest4 = contract_instance.call()._stage_invest4(Web3.toChecksumAddress(address))
        #     print(stage_invest4)
        #     locked_amount4 = stage_invest4[0]
        #     unlocked_amount4 = stage_invest4[1]
        #     dividends4 = stage_invest4[2]
        #     total_invest_stage4 = stage_invest4[3]
        #
        #     stage_invest5 = contract_instance.call()._stage_invest5(Web3.toChecksumAddress(address))
        #     print(stage_invest5)
        #     locked_amount5 = stage_invest5[0]
        #     unlocked_amount5 = stage_invest5[1]
        #     dividends5 = stage_invest5[2]
        #     total_invest_stage5 = stage_invest5[3]
        #
        #     stage_invest6 = contract_instance.call()._stage_invest6(Web3.toChecksumAddress(address))
        #     print(stage_invest6)
        #     locked_amount6 = stage_invest6[0]
        #     unlocked_amount6 = stage_invest6[1]
        #     dividends6 = stage_invest6[2]
        #     total_invest_stage6 = stage_invest6[3]
        #
        #     if len(address) == 42:
        #         logger.info(request.data)
        #         result = {
        #             "contract_address":contract_address,
        #             'general':{
        #             'active':active_invest,
        #             'total_invested':total_invest,
        #             'total_invest_ether':total_invest_ether,
        #             'locked_amount':locked_amount,
        #             'unlocked_amount':unlocked_amount,
        #             'dividends':0,
        #             'total_withdraw':total_withdraw,
        #             'signup':signup,
        #             },
        #             'stage1':{
        #                 'locked_amount':locked_amount0,
        #                 'unlocked_amount':unlocked_amount0,
        #                 'dividends':dividends0,
        #                 'stage_invest':total_invest_stage0
        #             },
        #             'stage2':{
        #                 'locked_amount':locked_amount1,
        #                 'unlocked_amount':unlocked_amount1,
        #                 'dividends':dividends1,
        #                 'stage_invest':total_invest_stage1
        #             },
        #             'stage3':{
        #                 'locked_amount':locked_amount2,
        #                 'unlocked_amount':unlocked_amount2,
        #                 'dividends':dividends2,
        #                 'stage_invest':total_invest_stage2
        #             },
        #             'stage4':{
        #                 'locked_amount':locked_amount3,
        #                 'unlocked_amount':unlocked_amount3,
        #                 'dividends':dividends3,
        #                 'stage_invest':total_invest_stage3
        #             },
        #             'stage5':{
        #                 'locked_amount':locked_amount4,
        #                 'unlocked_amount':unlocked_amount4,
        #                 'dividends':dividends4,
        #                 'stage_invest':total_invest_stage4
        #             },
        #             'stage6':{
        #                 'locked_amount':locked_amount5,
        #                 'unlocked_amount':unlocked_amount5,
        #                 'dividends':dividends5,
        #                 'stage_invest':total_invest_stage5
        #             },
        #             'stage7':{
        #                 'locked_amount':locked_amount6,
        #                 'unlocked_amount':unlocked_amount6,
        #                 'dividends':dividends6,
        #                 'stage_invest':total_invest_stage6
        #             }
        #         }
        #         results = result
        #         #results = InvestorsSerializer(result, many=True).data
        #         print(results)
        #         return Response(results)
        #     else:
        #         return Response("You have to use valid ether address !")
        # except:
        #         return Response("Your request is not valid! FU")

class StageInfo(APIView):
    """
    Get information about stage from stages
    address: valid ehter address length = 42
    parameters:
    - name: username
    """
    @classmethod
    def get_extra_actions(cls):
        return []

    def AbiParsingAndOutput(self,_outName,inputList,abi):
        test_abi = json.loads(abi)

        for item in test_abi:
            if 'name' in item.keys():
                #print(item['name'])
                if item['name'] == _outName:
                    i=0
                    result = {}
                    for in_item in item['outputs']:
                        print(in_item['name'],i)

                        result.update({in_item['name']:inputList[i]})
                        i += 1
        return result


    def get(self, request):
        # try:
        #http_provider = HTTPProvider('https://rinkeby.infura.io/v3/856fa4632365478692da1f0159040f73')
        http_provider = HTTPProvider('https://mainnet.infura.io/v3/856fa4632365478692da1f0159040f73')
        #http_provider = HTTPProvider('http://localhost:7545')
        web3 = Web3(http_provider)

        eth_provider = web3.middleware_stack.inject(geth_poa_middleware, layer=0)
        eth_provider = web3.eth
        contract_abi = settings.ABI
        contract_address = settings.DEFAULT_CONTRACT_ADDRESS
        #contract_address = models.ContractAndToken.objects.get(pk=1).contract_address

        contract_instance = eth_provider.contract(abi=contract_abi,address=Web3.toChecksumAddress(contract_address))
        result = {}
        current_stage = contract_instance.call().current_stage()
        token_cent = contract_instance.call().token_to_cent()
        token_rate = contract_instance.call().token_rate()
        bitcoin_rate = contract_instance.call().bitcoin_rate()
        current_date = contract_instance.call().current_date()
        ico_end_day = contract_instance.call().end_ico_day()
        ico_end_month = contract_instance.call().end_ico_month()
        ico_end_year = contract_instance.call().end_ico_year()
        stage_count = contract_instance.call().stage_count()
        ico_end = contract_instance.call().ico_end()
        vesting_date_count = contract_instance.call().vesting_date_count()
        general_limit_stage = contract_instance.call().general_limit_stage()
        stop_contract_state = contract_instance.call().stop_contract_state()
        dividends_persent = contract_instance.call().dividends_persent()
        result.update({
                    'ico_end':ico_end,
                    'ico_end_year':ico_end_year,
                    'ico_end_month':ico_end_month,
                    'ico_end_day':ico_end_day,
                    'current_date':current_date,
                    'contract_address':contract_address,
                    'current_stage':current_stage,
                    'stage_count':stage_count,
                    'token_cent':token_cent,
                    'ether_rate':token_rate,
                    'bitcoin_rate':bitcoin_rate,
                    'dividends_persent':dividends_persent,
                    'vesting_date_count':vesting_date_count,
                    'investing_limit':general_limit_stage,
                    'stop_contract_state':stop_contract_state})
        for stage_number in range(1,9):
            current_stage = contract_instance.call()._stage(stage_number)
            print(current_stage)
            current_stage_key = "stage"+str(stage_number)
            print(result.update({current_stage_key:self.AbiParsingAndOutput('_stage',current_stage,contract_abi)}))
        return Response(result)
        # contract_instance = eth_provider.contract(abi=contract_abi,address=Web3.toChecksumAddress(contract_address))
        # current_stage = contract_instance.call().current_stage()
        # stage_count = contract_instance.call().stage_count()
        # print(current_stage)
        # stage0_set = contract_instance.call().stage0_setting()
        # active0 = stage0_set[0]
        # start_date0  = stage0_set[1]
        # bonus0  = stage0_set[2]
        # duration0  = stage0_set[3]
        # sold_token0  = stage0_set[4]
        # vesting_time0  = stage0_set[5]
        # min0  = stage0_set[6]
        # max0  = stage0_set[7]
        # total_tokens0 = stage0_set[8]
        # total_investors0 = stage0_set[10]
        #
        #
        # stage1_set = contract_instance.call().stage1_setting()
        # active1 = stage1_set[0]
        # start_date1  = stage1_set[1]
        # bonus1  = stage1_set[2]
        # duration1  = stage1_set[3]
        # sold_token1  = stage1_set[4]
        # vesting_time1  = stage1_set[5]
        # min1  = stage1_set[6]
        # max1  = stage1_set[7]
        # total_tokens1 = stage1_set[8]
        # total_investors1 = stage1_set[10]
        #
        # stage2_set = contract_instance.call().stage2_setting()
        # active2 = stage2_set[0]
        # start_date2  = stage2_set[1]
        # bonus2  = stage2_set[2]
        # duration2  = stage2_set[3]
        # sold_token2  = stage2_set[4]
        # vesting_time2  = stage2_set[5]
        # min2  = stage2_set[6]
        # max2  = stage2_set[7]
        # total_tokens2 = stage2_set[8]
        # total_investors2 = stage2_set[10]
        #
        # stage3_set = contract_instance.call().stage3_setting()
        # active3 = stage3_set[0]
        # start_date3  = stage3_set[1]
        # bonus3  = stage3_set[2]
        # duration3  = stage3_set[3]
        # sold_token3  = stage3_set[4]
        # vesting_time3  = stage3_set[5]
        # min3  = stage3_set[6]
        # max3  = stage3_set[7]
        # total_tokens3 = stage3_set[8]
        # total_investors3 = stage3_set[10]
        #
        # stage4_set = contract_instance.call().stage4_setting()
        # active4 = stage4_set[0]
        # start_date4  = stage4_set[1]
        # bonus4  = stage4_set[2]
        # duration4  = stage4_set[3]
        # sold_token4  = stage4_set[4]
        # vesting_time4  = stage4_set[5]
        # min4  = stage4_set[6]
        # max4  = stage4_set[7]
        # total_tokens4 = stage4_set[8]
        # total_investors4 = stage4_set[10]
        #
        # stage5_set = contract_instance.call().stage5_setting()
        # active5 = stage5_set[0]
        # start_date5  = stage5_set[1]
        # bonus5  = stage5_set[2]
        # duration5  = stage5_set[3]
        # sold_token5  = stage5_set[4]
        # vesting_time5  = stage5_set[5]
        # min5  = stage5_set[6]
        # max5  = stage5_set[7]
        # total_tokens5 = stage5_set[8]
        # total_investors5 = stage5_set[10]
        #
        # stage6_set = contract_instance.call().stage6_setting()
        # active6 = stage6_set[0]
        # start_date6  = stage6_set[1]
        # bonus6  = stage6_set[2]
        # duration6  = stage6_set[3]
        # sold_token6  = stage6_set[4]
        # vesting_time6  = stage6_set[5]
        # min6  = stage6_set[6]
        # max6  = stage6_set[7]
        # total_tokens6 = stage6_set[8]
        # total_investors6 = stage6_set[10]
        #
        # result = {
        #     "contract_address":contract_address,
        #     "current_stage":current_stage,
        #     "stage_count":stage_count,
        #     "stage1":{
        #         'active':active0,
        #         'bonus':bonus0,
        #         'start_date':start_date0,
        #         'total_tokens':total_tokens0,
        #         'duration':duration0,
        #         'sold_token':sold_token0,
        #         'vesting_time':vesting_time0,
        #         'min':min0,
        #         'max':max0,
        #         'total_investors':total_investors0},
        #     "stage2":{
        #         'active':active1,
        #         'bonus':bonus1,
        #         'start_date':start_date1,
        #         'total_tokens':total_tokens1,
        #         'duration':duration1,
        #         'sold_token':sold_token1,
        #         'vesting_time':vesting_time1,
        #         'min':min1,
        #         'max':max1,
        #         'total_investors':total_investors1},
        #     "stage3":{
        #         'active':active2,
        #         'bonus':bonus2,
        #         'start_date':start_date2,
        #         'total_tokens':total_tokens2,
        #         'duration':duration2,
        #         'sold_token':sold_token2,
        #         'vesting_time':vesting_time2,
        #         'min':min2,
        #         'max':max2,
        #         'total_investors':total_investors2},
        #     "stage4":{
        #         'active':active3,
        #         'bonus':bonus3,
        #         'start_date':start_date3,
        #         'total_tokens':total_tokens3,
        #         'duration':duration3,
        #         'sold_token':sold_token3,
        #         'vesting_time':vesting_time3,
        #         'min':min3,
        #         'max':max3,
        #         'total_investors':total_investors3},
        #     "stage5":{
        #         'active':active4,
        #         'bonus':bonus4,
        #         'start_date':start_date4,
        #         'total_tokens':total_tokens4,
        #         'duration':duration4,
        #         'sold_token':sold_token4,
        #         'vesting_time':vesting_time4,
        #         'min':min4,
        #         'max':max4,
        #         'total_investors':total_investors4},
        #     "stage6":{
        #         'active':active5,
        #         'bonus':bonus5,
        #         'start_date':start_date5,
        #         'total_tokens':total_tokens5,
        #         'duration':duration5,
        #         'sold_token':sold_token5,
        #         'vesting_time':vesting_time5,
        #         'min':min5,
        #         'max':max5,
        #         'total_investors':total_investors5},
        #     "stage7":{
        #         'active':active6,
        #         'bonus':bonus6,
        #         'start_date':start_date6,
        #         'total_tokens':total_tokens6,
        #         'duration':duration6,
        #         'sold_token':sold_token6,
        #         'vesting_time':vesting_time6,
        #         'min':min6,
        #         'max':max6,
        #         'total_investors':total_investors6}
        # }

        # return Response(result)
        # except:
        #     return Response("Your request is not valid! FU")


class PaymentsEvents(APIView):
    """
    Get information payments transaction by address
    in json txn- (number of transaction in blockchain other words ID of transaction)
    in this endpoint you'll get all transaction witch is connected to  smart contract
    In some cases it can take some time

    """
    @classmethod
    def get_extra_actions(cls):
        return []

    def get(self, request):
        try:
            #http_provider = HTTPProvider('https://rinkeby.infura.io/v3/856fa4632365478692da1f0159040f73',request_kwargs={'timeout': 60})
            web3 = Web3(Web3.WebsocketProvider("wss://mainnet.infura.io/ws", websocket_kwargs={'timeout': 60}))
            #http_provider = HTTPProvider('http://localhost:7545')

            contract_abi = settings.ABI
            contract_address = settings.DEFAULT_CONTRACT_ADDRESS
            #contract_address = models.ContractAndToken.objects.get(pk=1).contract_address
            web3.middleware_stack.inject(geth_poa_middleware, layer=0)
            myContract = web3.eth.contract(abi=contract_abi,address=Web3.toChecksumAddress(contract_address))
            #event_signature_topic = event_abi_to_log_topic(myContract.events.Payment.abi)
            event_filter = myContract.events.Payment.createFilter(fromBlock=300)
            event_list = event_filter.get_all_entries()
            main_dict = {}
            for item in event_list:
                print(item.logIndex)
                txn = 'txn-'+str(item.logIndex)
                item = item.args
                dict = {
                    "bitcoin_address":item._bitcoin,
                    "investor_address":item._from,
                    "data":item._date,
                    "ether_amount":item._ether_amount,
                    "btc_amount":item._btc_amount,
                    "stage":item._round,
                    "tokens":item._tokens,
                    "type":item._type
                    }

                main_dict.update({txn:dict})


            return Response(main_dict)
        except:
            return Response("Your request is not valid! FU")


class PaymentsEventsAddress(APIView):
    """
    Get information payments transaction by address
    in json txn- (number of transaction in blockchain other words ID of transaction)
    -address: ether address
    HOW TO USE
    /api/events/<address of ivestor>
    """


    @classmethod
    def get_extra_actions(cls):
        return []
    def get(self, request,address):
        try:
            web3 = Web3(Web3.WebsocketProvider("wss://mainnet.infura.io/ws", websocket_kwargs={'timeout': 60}))
            #http_provider = HTTPProvider('http://localhost:7545')

            contract_abi = settings.ABI
            contract_address = settings.DEFAULT_CONTRACT_ADDRESS
            #contract_address = models.ContractAndToken.objects.get(pk=1).contract_address
            web3.middleware_stack.inject(geth_poa_middleware, layer=0)
            myContract = web3.eth.contract(abi=contract_abi,address=Web3.toChecksumAddress(contract_address))
            #event_signature_topic = event_abi_to_log_topic(myContract.events.Payment.abi)
            event_filter = myContract.events.Payment.createFilter(fromBlock=300,argument_filters={'_from':address})
            event_list = event_filter.get_all_entries()
            main_dict = {}
            for item in event_list:
                print(item.logIndex)
                txn = 'txn-'+str(item.logIndex)
                item = item.args
                dict = {
                    "bitcoin_address":item._bitcoin,
                    "investor_address":item._from,
                    "data":item._date,
                    "ether_amount":item._ether_amount,
                    "btc_amount":item._btc_amount,
                    "tokens":item._tokens,
                    "stage":item.current_stage,
                    "type":item._type
                    }

                main_dict.update({txn:dict})


            return Response(main_dict)
        except:
            return Response("Your request is not valid! FU")


class CurrentTechInfo(APIView):
    """
    Get aci of contract
    """


    @classmethod
    def get_extra_actions(cls):
        return []
    def get(self, request):
        contract_abi = settings.ABI
        abi = {"abi":contract_abi}
        return Response(json.loads(contract_abi))
